if (!String.prototype.endsWith)
  String.prototype.endsWith = function(searchStr, Position) {
    if (!(Position < this.length)) Position = this.length;
    else Position |= 0; // round position
    return (
      this.substr(Position - searchStr.length, searchStr.length) === searchStr
    );
  };

if (
  window.location.href.endsWith("/hc/ja/community") ||
  window.location.href.endsWith("/hc/ja/community/topics")
) {
  window.location.href =
    "/hc/ja/community/topics/360001163393-%E3%82%B3%E3%83%9F%E3%83%A5%E3%83%8B%E3%83%86%E3%82%A3%E3%83%95%E3%82%A9%E3%83%BC%E3%83%A9%E3%83%A0-Japanese-Forum";
}

if (window.location.href.indexOf("/community/") > -1) {
  $("html").addClass("community-page");
}

if (window.location.href.indexOf("/community/posts/115002924609") > -1) {
  window.location.href = "https://status.wrike.com/";
}

if (document.location.href.indexOf("filter_by=community") > -1) {
  $("html").addClass("community-page");
}
if (window.location.href.indexOf("/categories/201196365") > -1) {
  window.location.href =
    "/hc/en-us/community/topics/115000360569-Weekly-Release-Notes";
}

var oldIds = [
  "209735549",
  "212281265",
  "210322745",
  "209603669",
  "209603849",
  "360000291365"
];
var newIds = [
  "115003869085",
  "360001871874#change",
  "209602969",
  "209602969",
  "209602969",
  "360000209885"
];
var lang = document
  .getElementsByTagName("html")[0]
  .getAttribute("lang")
  .toLowerCase();
for (var i = 0; i < oldIds.length; i++) {
  if (window.location.href.indexOf(oldIds[i]) > -1) {
    window.location.href =
      "https://help.wrike.com/hc/" + lang + "/articles/" + newIds[i];
  }
}
// console.log( 'Filter by: '+ document.location.href.indexOf('filter_by=community') );
if (
  window.location.href.indexOf("/search?") > -1 &&
  document.location.href.indexOf("filter_by=community") === -1
) {
  if (localStorage.getItem("searchScope") === "cm") {
    console.log(localStorage.getItem("searchScope"));
    localStorage.setItem("searchScope", "");
    location.href = location.href.replace(
      /search?/g,
      "search?filter_by=community&"
    );
  }
} else {
  if (window.location.href.indexOf("/community/") > -1) {
    localStorage.setItem("searchScope", "cm");
  } else {
    localStorage.setItem("searchScope", "");
  }
}

//iframe new post
var url = window.location.href;
var lastPart = url.substr(url.lastIndexOf("/") + 1);

if (
  window.location.href.indexOf("/community/posts/new") > -1 ||
  lastPart === "posts"
) {
  if (window.location !== window.parent.location) {
    $("html").addClass("new-post-iframe");
  } // if iframe end
} else if (window.location.href.indexOf("/community/posts/") > -1) {
  if (window.location !== window.parent.location) {
    $("html").addClass("post-iframe");
    var sectionId = localStorage.getItem("sectionId");
    var articleTitle = localStorage.getItem("articleTitle");
    window.top.location.href =
      window.location.href +
      "?post_source=article&section=" +
      sectionId +
      "&article=" +
      articleTitle;
  } // if iframe end
} //if new post page end

$(document).ready(function() {
  $('a[href$="/hc/ja/community/topics"]').attr(
    "href",
    "/hc/ja/community/topics/360001163393-%E3%82%B3%E3%83%9F%E3%83%A5%E3%83%8B%E3%83%86%E3%82%A3%E3%83%95%E3%82%A9%E3%83%BC%E3%83%A9%E3%83%A0-Japanese-Forum"
  );

  $("input#query").on("change keydown paste input", function() {
    $("body > *:last-child").addClass("topns:");
  });

  if (window.location.href.indexOf("/articles/") > -1) {
    var secId = $("ol.breadcrumbs li:last-child a")
      .attr("href")
      .split("/sections/")[1];
    var articleTitle = $('link[rel="canonical"]')
      .attr("href")
      .split("/articles/")[1];
    localStorage.setItem("sectionId", secId);
    localStorage.setItem("articleTitle", articleTitle);
  }
  $(".moderate-string").remove();
  $("#radio-form input").on("change", function() {
    $("#radio-form input")
      .not(this)
      .prop("checked", false);
  });

  $(
    '.community-follow [role=button], .community-follow.wide-button a.post-unsubscribe, a[data-action="edit-communitypost"], a[data-action="edit-communitycomment"], a[data-action="escalate"],.custom-follow > a'
  ).click(function(event) {
    event.preventDefault();
    console.log("prevent Default");
  });

  //set cookie if click on article from article page in case article is not translated
  $(".article-body a[href*=articles]").on("click", function() {
    var linkfrom = $(this).attr("href");
    // localStorage.setItem('article_from_article_page', linkfrom);
    $.cookie("article_from_article_page", linkfrom, {
      expires: 1 / 24 / 6 / 12,
      path: "/"
    });
    console.log($.cookie("article_from_article_page"));
    document.cookie = "username=John Doe";
  });
  //in case if opens a link from the right click menu
  $(".article-body a[href*=articles]").contextmenu(function() {
    var linkfrom = $(this).attr("href");
    $.cookie("article_from_article_page", linkfrom, {
      expires: 1 / 24 / 6 / 12,
      path: "/"
    });
  });
  //new community post page, drop-down set topic title + desc
  listadiv = $("body.new-community-post-page > .nesty-panel");
  $("a.nesty-input").on("click", function() {
    $(listadiv)
      .find("li:not(#null)")
      .each(function() {
        var id = $(this).attr("id"),
          text = localStorage.getItem(id);
        $(this).text(text);
      });
  });

  if (HelpCenter.user.role == "manager") {
    $("body").addClass("manager");
  }

  if (HelpCenter.user.role == "agent") {
    $("body").addClass("agent");
  }

  if (HelpCenter.user.role == "end_user") {
    $("body").addClass("end-user");
    $(".subs").remove();
  }

  if (HelpCenter.user.role == "anonymous") {
    $("body").addClass("anonymous");
    $(".subs").remove();
  }

  // CA answers code
  $(".ca_string")
    .closest(".comment-wrapper")
    .addClass("ca");
  $("p.ca_spot")
    .closest(".comment-wrapper")
    .addClass("ca_spot")
    .removeClass("ca_adv, ca_inn");
  $("p.ca_inn")
    .closest(".comment-wrapper")
    .addClass("ca_inn")
    .removeClass("ca_spot, ca_adv");
  $("p.ca_adv")
    .closest(".comment-wrapper")
    .addClass("ca_adv")
    .removeClass("ca_spot, ca_inn");
  $(
    'body:not(.manager) .ca-btns, body:not(.manager) .comment-wrapper.ca + div .actions a[data-action="edit-communitycomment"]'
  ).remove();
  $("body:not(.manager) .ca_string").removeClass(
    "ca_adv ca_inn ca_spot ca_string"
  );

  // set cookie if user manually change lang for no redirection
  $(".language-selector span a").click(function() {
    $.cookie("lang-set", true, { expires: 99 });
  });

  // remove vote down on product feedback topic posts
  $(".kudos a.vote-down").remove();

  // new post in community product feedback - popup
  $(".popup-container img").on("click", function() {
    $("body").removeClass("new-best-practice-post");
  });

  localStorage.removeItem("topicSelected");
  if (window.location.href.indexOf("/community/posts/new") >= 0) {
    var topic = $("form.new_community_post a.nesty-input").text();
    localStorage.setItem("topicSelected", topic);
    $("select#community_post_topic_id").on("change", function() {
      var topic = $("a.nesty-input").text();
      localStorage.setItem("topicSelected", topic);
    });
  }

  // lang switcher if link is ending with return to lang HP, update link cause page is not translated
  $(
    'body:not(.homepage) .dropdown.language-selector a[href$="return_to=%2Fhc%2Fde"]'
  ).attr("href", function(i, href) {
    return href + "?no-translated-page";
  });

  $(
    'body:not(.homepage) .dropdown.language-selector a[href$="return_to=%2Fhc%2Fes"]'
  ).attr("href", function(i, href) {
    return href + "?no-translated-page";
  });

  $(
    'body:not(.homepage) .dropdown.language-selector a[href$="return_to=%2Fhc%2Ffr"]'
  ).attr("href", function(i, href) {
    return href + "?no-translated-page";
  });

  $(
    'body:not(.homepage) .dropdown.language-selector a[href$="return_to=%2Fhc%2Fit"]'
  ).attr("href", function(i, href) {
    return href + "?no-translated-page";
  });

  $(
    'body:not(.homepage) .dropdown.language-selector a[href$="return_to=%2Fhc%2Fru"]'
  ).attr("href", function(i, href) {
    return href + "?no-translated-page";
  });

  $(
    'body:not(.homepage) .dropdown.language-selector a[href$="return_to=%2Fhc%2Fpt-br"]'
  ).attr("href", function(i, href) {
    return href + "?no-translated-page";
  });

  $(
    'body:not(.homepage) .dropdown.language-selector a[href$="return_to=%2Fhc%2Fja"]'
  ).attr("href", function(i, href) {
    return href + "?no-translated-page";
  });

  // social share popups
  $(".share a").click(function(e) {
    e.preventDefault();
    window.open(this.href, "", "height = 500, width = 500");
  });

  // toggle the share dropdown in communities
  $(".share-label").on("click", function(e) {
    e.stopPropagation();
    var isSelected = this.getAttribute("aria-selected") == "true";
    this.setAttribute("aria-selected", !isSelected);
    $(".share-label")
      .not(this)
      .attr("aria-selected", "false");
  });

  $(document).on("click", function() {
    $(".share-label").attr("aria-selected", "false");
  });

  // show form controls when the textarea receives focus or backbutton is used and value exists
  var $commentContainerTextarea = $(".comment-container textarea"),
    $commentContainerFormControls = $(".comment-form-controls, .comment-ccs");

  $commentContainerTextarea.one("focus", function() {
    $commentContainerFormControls.show();
  });

  if ($commentContainerTextarea.val() !== "") {
    $commentContainerFormControls.show();
  }

  // Submit requests filter form in the request list page
  $("#request-status-select, #request-organization-select").on(
    "change",
    function() {
      search();
    }
  );

  // Submit requests filter form in the request list page
  $("#quick-search").on("keypress", function(e) {
    if (e.which === 13) {
      search();
    }
  });

  function search() {
    window.location.search = $.param({
      query: $("#quick-search").val(),
      status: $("#request-status-select").val(),
      organization_id: $("#request-organization-select").val()
    });
  }

  // Submit organization form in the request page
  $("#request-organization select").on("change", function() {
    this.form.submit();
  });

  // Social sharing tracking
  $(".share a").on("click", function(e) {
    var $this = $(this),
      type = $this.attr("class").replace("share-", ""),
      path = window.location.pathname;
    ga("_trackEvent", "Social Share", type, path);
  });

  $('label:contains("Link")').addClass("label-block");

  $.fn.isVisible = function() {
    // Current distance from the top of the page
    var windowScrollTopView = $(window).scrollTop();

    // Current distance from the top of the page, plus the height of the window
    var windowBottomView = windowScrollTopView + $(window).height();

    // Element distance from top
    var elemTop = $(this).offset().top;

    // Element distance from top, plus the height of the element
    var elemBottom = elemTop + $(this).height();

    return elemBottom <= windowBottomView && elemTop >= windowScrollTopView;
  };
  // float Ask wrike btn above the footer
  $(window).scroll(function() {
    if ($(".chat-helper").isVisible()) {
      $("a.ask-wrike").addClass("show-btn");
    } else {
      $("a.ask-wrike").removeClass("show-btn");
    }
  });

  // article page mobile menu select article from list
  $(".collapsible-nav, .collapsible-sidebar").on("click", function(e) {
    e.stopPropagation();
    var isExpanded = this.getAttribute("aria-expanded") === "true";
    this.setAttribute("aria-expanded", !isExpanded);
  });
  $(".collapsible-sidebar ul").prepend(
    '<li><a class="sidenav-item sidenav-select">-- select --</a></li>'
  );

  // hamburger menu
  $(".header .icon-menu").on("click", function(e) {
    e.stopPropagation();
    var menu = document.getElementById("user-nav");
    var isExpanded = menu.getAttribute("aria-expanded") === "true";
    menu.setAttribute("aria-expanded", !isExpanded);
  });

  if ($("#user-nav").children().length === 0) {
    $(".header .icon-menu").hide();
  }

  // show form controls when the textarea receives focus or backbutton is used and value exists
  var $commentContainerTextarea = $(".comment-container textarea"),
    $commentContainerFormControls = $(".comment-form-controls, .comment-ccs");

  $commentContainerTextarea.one("focus", function() {
    $commentContainerFormControls.show();
  });

  if ($commentContainerTextarea.val() !== "") {
    $commentContainerFormControls.show();
  }

  //Up Arrow
  var offset = 300,
    offset_opacity = 1200,
    scroll_top_duration = 700,
    $back_to_top = $(".cd-top");
  $(window).scroll(function() {
    $(this).scrollTop() > offset
      ? $back_to_top.addClass("cd-is-visible")
      : $back_to_top.removeClass("cd-is-visible cd-fade-out");
    if ($(this).scrollTop() > offset_opacity) {
      $back_to_top.addClass("cd-fade-out");
    }
  });

  //smooth scroll to top
  $back_to_top.on("click", function(event) {
    event.preventDefault();
    $("body,html").animate(
      {
        scrollTop: 0
      },
      scroll_top_duration
    );
  });

  $(".popup-container img, #popup-overlay").click(function(e) {
    e.preventDefault();
    $("body").removeClass("enable-popup-page");
  });

  $(".popup-container img, #popup-overlay").click(function(e) {
    e.preventDefault();
    $("body").removeClass("enable-popup");
  });

  $(".ten-min-community-alert img, body.less-darker div#popup-overlay").click(
    function(e) {
      e.preventDefault();
      localStorage.setItem("timeSpentPrev", "0");
      $("body").removeClass("enable-popup-page enable-popup");
      $(".ten-min-community-alert, #popup-overlay").remove();
    }
  );

  $(".five-min-alert img, body.less-darker div#popup-overlay").click(function(
    e
  ) {
    $("body").removeClass("enable-popup-page enable-popup");
    $(".five-min-alert, #popup-overlay").remove();
  });

  $('.ask_wrike footer.post-page input[type="submit"]').prop("disabled", true);
  // custom banner set cookie
  var bannerid = $(".video-tutorials-hp.custom-banner").attr("id");
  var cookid = bannerid + "-noShowBanner";
  $(".video-tutorials-hp.custom-banner span").click(function() {
    $.cookie(cookid, true, { expires: 99 });
  });
  // custom banner hide on X click
  $(".video-tutorials-hp.custom-banner span").click(function(e) {
    e.preventDefault();
    $(this)
      .closest(".custom-banner")
      .addClass("closed");
  });
  $(".video-tutorials-hp.custom-banner-two span").click(function(e) {
    e.preventDefault();
    $(this)
      .closest(".custom-banner-two")
      .remove();
  });
  console.log("ie test");

  // find all YT links in Community comments/posts and replace with YT iframe
  $(
    '.comment-body p a[href^="https://www.youtube.com/"], .post-body a[href^="https://www.youtube.com/"]'
  ).each(function() {
    var url = $(this).attr("href");
    function getId(url) {
      var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
      var match = url.match(regExp);
      if (match && match[2].length == 11) {
        return match[2];
      } else {
        return "error";
      }
    }

    var myId = getId(url);
    $(this).html(
      '<iframe width="560" height="315" src="//www.youtube.com/embed/' +
        myId +
        '" frameborder="0" allowfullscreen></iframe>'
    );
  });

  // find all Vimeo links in Community comments/posts and replace with an iframe
  $('.comment-body p a[href*="vimeo"], .post-body a[href*="vimeo"]').each(
    function() {
      var url = $(this).attr("href");

      function getVimeoId(url) {
        var m = url.match(/^.+vimeo.com\/(.*\/)?([^#\?]*)/);
        return m ? m[2] || m[1] : null;
      }

      var myId = getVimeoId(url);
      console.log(myId);
      $(this).html(
        '<div style="padding:330px 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/' +
          myId +
          '" style="position:absolute;top:0;left:0;width:560px;height:315px;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>'
      );
    }
  );

  // find all Wistia links in Community comments/posts and replace with an iframe
  $('.comment-body p a[href*="wvideo"], .post-body a[href*="wvideo"]').each(
    function() {
      var url = $(this).attr("href");
      var video_id = url.split("=")[1];
      $(this).html(
        '<p><iframe class="wistia_embed" src="//fast.wistia.net/embed/iframe/' +
          video_id +
          '" name="wistia_embed" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen=""></iframe></p><script src="//fast.wistia.net/assets/external/E-v1.js" async=""></script>'
      );
    }
  );

  $(".ten-min-community-alert p a:first-child").on("click", function() {
    //Sign-in with Wrike
    window.dispatchEvent(new CustomEvent("tenminFirstlink"));
  });

  $(".ten-min-community-alert p a:nth-child(2)").on("click", function() {
    //Start a free Wrike Trial
    window.dispatchEvent(new CustomEvent("tenminSecondlink"));
  });

  $(".five-min-alert p a:first-child").on("click", function() {
    //Ask the Wrike Community
    window.dispatchEvent(new CustomEvent("fiveminFirstlink"));
  });

  //remove post source from article page for users
  $(".anonymous span.post-source, .end_user span.post-source").remove();

  if ($(".ask-popup").length) {
    $(".ask-popup #community_post_title").attr(
      "placeholder",
      "Ask your question here"
    );
    $('.ask-popup .post-page input[type="submit"]').val("Ask");
    $('.ask-popup [for="community_post_topic_id"] + a')[0].click();
    $(".ask-popup body > .nesty-panel li:nth-child(5)").click();
  }

  //float banner
  if (HelpCenter.user.role != "anonymous") {
    $(".banner-username").text(HelpCenter.user.name.split(" ")[0]);
  }

  $("span.close-float-banner").on("click", function() {
    $(this)
      .closest(".float-banner")
      .remove();
    $.cookie("floatbanner", 1, { path: "/hc/" });
  });
  $(".new-post-iframe .float-banner").remove();

  var url_string = window.location.href;
  var url = new URL(url_string);
  var subject = url.searchParams.get("subject");
  $("input#request_subject").val(subject);

  $(".ask-comm-article-nav").on("click", function(e) {
    e.preventDefault();
    $("html,body").animate(
      {
        scrollTop: $(".new-post-frame").offset().top
      },
      "slow"
    );
  });

  //moderate community post from some wrike members
  var loginHref = $("a.login").attr("href");
  $(".ask-comm-anon .login-new-post").attr("href", loginHref);

  // doc.ready END
});

$(document).ready(function() {
  if (
    HelpCenter.user.email == "wlada@gmail.com" ||
    HelpCenter.user.email == "wlada@gmail.com"
  ) {
    console.log("ovo je email");
  } else {
    (function() {
      var isCust;
      // find the tag in the array
      function isCustomer(element, index, array) {
        return element === "xxxwrike_employee";
      }
      //go through the HelpCenter object and look for org tags
      HelpCenter.user.organizations.forEach(function(x) {
        isCust = x.tags.some(isCustomer);
        return isCust === true;
      });
      //is this a customer and show them START
      if (isCust === true) {
        setTimeout(function() {
          $("#community_post_details_ifr").ready(function() {
            setTimeout(function() {
              head = $("#community_post_details_ifr")
                .contents()
                .find("head");
              $(head).append(
                '<style type="text/css">span.moderate-string{visibility:hidden;position:absolute;top:-9999px;color:transparent!important}</style>'
              );
            }, 2000);
          });
        }, 2000);

        setTimeout(function() {
          $("#community_comment_body_ifr").ready(function() {
            setTimeout(function() {
              head = $("#community_comment_body_ifr")
                .contents()
                .find("head");
              $(head).append(
                '<style type="text/css">span.moderate-string{visibility:hidden;position:absolute;top:-9999px;color:transparent!important}</style>'
              );
            }, 2000);
          });
        }, 2000);

        //for new posts
        $('.new_community_post [type="submit"]').mousedown(function() {
          head = $("#community_post_details_ifr")
            .contents()
            .find("head");
          $(head).append(
            '<style type="text/css">span.moderate-string{visibility:hidden;position:absolute;top:-9999px;color:transparent!important}</style>'
          );
          var moderateString = $(
            '<p><span class="moderate-string">mdr</span></p>'
          ).html();
          var body = document.getElementById("community_post_details_ifr")
            .contentWindow.document.body;
          $(body).append(moderateString);
        });

        //for new comments
        $(".comment-form-controls input[type=submit]").mousedown(function() {
          head = $("#community_comment_body_ifr")
            .contents()
            .find("head");
          $(head).append(
            '<style type="text/css">span.moderate-string{visibility:hidden;position:absolute;top:-9999px;color:transparent!important}</style>'
          );
          var moderateString = $(
            '<p><span class="moderate-string">mdr</span></p>'
          ).html();
          var body = document.getElementById("community_comment_body_ifr")
            .contentWindow.document.body;
          var style = $(document.getElementById("community_comment_body_ifr"))
            .contents()
            .find("head")
            .append("<style>.moderate-text{display:none;}</style>");
          $(body).append(moderateString);
        });
      }
      //is this a customer and show them END
    })();
  }

  $(".subs > span").on("click", function() {
    $(".subs").toggleClass("active");
  });

  var floatBannerText = [1, 2, 3, 4, 5, 6];
  $(
    ".float-banner section p:nth-child(" +
      floatBannerText[Math.floor(Math.random() * floatBannerText.length)] +
      ")"
  ).css("display", "block");

  //doc ready end
});

(function() {
  var usDomain = "www.wrike.com";
  var us2Domain = "app-us2.wrike.com";
  var euDomain = "app-eu.wrike.com";

  function StatusResponse(data) {
    this.showLiveChat = data.showLiveChat;
    this.showPremiumLiveChat = data.premiumLiveChat;
  }

  var helpCenterDepartment = "Help Center";
  var premiumDepartment = "Premium Support";

  var helpCenterTag = "help_center";
  var premiumSupportTag = "premium_chat";

  function fetchChatStatus(domain) {
    var statusCheckerServlet = "https://" + domain + "/ui/live_chat_status";
    return fetch(statusCheckerServlet, {
      headers: {
        "wrike-client-id": 1,
        "x-w-client": "app:helpcenter;ver:1.0.0-1"
      },
      credentials: "include",
      method: "post"
    })
      .then(function(data) {
        return data.json();
      })
      .then(function(json) {
        return new StatusResponse(json.data);
      });
  }

  function getChatStatus() {
    return fetchChatStatus(usDomain)
      .catch(() => fetchChatStatus(euDomain))
      .catch(() => fetchChatStatus(us2Domain))
      .catch(() => {});
  }

  function disableLiveChat() {
    zE("webWidget", "updateSettings", {
      webWidget: { chat: { suppress: true } }
    });
  }

  function setUserDepartment(department) {
    $zopim.livechat.departments.setVisitorDepartment(department);
    $zopim.livechat.departments.filter("");
  }

  function isDepartmentOnline(checkedDepartment) {
    var department = $zopim.livechat.departments.getDepartment(
      checkedDepartment
    );
    return department && department.status === "online";
  }

  function activateHelpCenterChat() {
    $zopim.livechat.addTags(helpCenterTag);
    setUserDepartment(helpCenterDepartment);
  }

  function activatePremiumChat() {
    $zopim.livechat.addTags([premiumSupportTag, helpCenterTag]);
    setUserDepartment(premiumDepartment);
  }

  function initLiveChat() {
    if ($zopim.livechat.isChatting()) {
      zE.activate();
      return;
    }

    getChatStatus().then(function(chatStatus) {
      if (!chatStatus) {
        disableLiveChat();
        return;
      }

      if (chatStatus.showPremiumLiveChat) {
        activatePremiumChat();
        return;
      }

      if (!isDepartmentOnline(helpCenterDepartment)) {
        disableLiveChat();
        return;
      }

      if (!chatStatus.showLiveChat) {
        disableLiveChat();
        return;
      }

      activateHelpCenterChat();
    });
  }

  zE(function() {
    $zopim(function() {
      $zopim.livechat.setOnConnected(initLiveChat);
    });
  });
})();

$(document).ready(function() {
  //voted JS hide if already voted
  $('.forum a.vote-up:not([aria-selected="true"])').each(function() {
    $(this)
      .parent()
      .addClass("not-voted");
  });
  $('.forum a.vote-up:not([aria-selected="true"])').on("click", function() {
    like = this;
    $(like)
      .parent()
      .addClass("just-voted");
    $(like)
      .prev()
      .slideUp(150, function() {
        setTimeout(function() {
          $(like)
            .prev()
            .text("Done!")
            .slideDown();
        }, 200);
      });
  });
});

function getTopDomain() {
  return (
    "." +
    (
      window.location.host.match(/[a-z0-9-]+\.[a-z0-9-]{2,3}$/) || [
        window.location.host
      ]
    ).pop()
  );
}

$(document).ready(function() {
  var COOKIE_LANG = {
    name: "wrikeLocale",
    domain: getTopDomain(),
    duration: 365 * 100,
    path: "/"
  };

  var HC_TO_SITE_LOCALES = {
    "en-us": "en",
    de: "de",
    es: "es",
    fr: "fr",
    it: "it",
    ja: "ja",
    "pt-br": "pt-br",
    ru: "ru"
  };

  var REGULAR_EXPRESSIONS = {
    current: new RegExp(
      "/hc/(" + Object.keys(HC_TO_SITE_LOCALES).join("|") + ")",
      "i"
    ),
    target: new RegExp(
      "/hc/change_language/(" + Object.keys(HC_TO_SITE_LOCALES).join("|") + ")",
      "i"
    )
  };

  function setWrikeLocaleCookie(locale) {
    if ($.cookie(COOKIE_LANG.name) !== locale) {
      $.cookie(COOKIE_LANG.name, locale, {
        domain: COOKIE_LANG.domain,
        expires: COOKIE_LANG.duration,
        path: COOKIE_LANG.path
      });
    }
  }

  function setCookieByPath(targetPath, localeRegExp) {
    var targetPathnameLocale = (localeRegExp.exec(targetPath) || [])[1];

    if (targetPathnameLocale in HC_TO_SITE_LOCALES) {
      setWrikeLocaleCookie(HC_TO_SITE_LOCALES[targetPathnameLocale]);
    }
  }

  // Initial cookie setting
  setCookieByPath(window.location.pathname, REGULAR_EXPRESSIONS.current);

  // Set cookie after language change
  $("#language-selector-header a, #language-selector-footer a").click(function(
    event
  ) {
    setCookieByPath(event.target.pathname, REGULAR_EXPRESSIONS.target);
  });
});
